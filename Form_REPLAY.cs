﻿using System;
using System.Linq;
using System.Windows.Forms;
using AXISMEDIACONTROLLib;
using LibVLCSharp.Shared;

namespace WindowsFormsReplay
{
    public partial class Form_MAIN : Form
    {
        readonly System.Collections.Generic.HashSet<RendererItem> _rendererItems = new System.Collections.Generic.HashSet<RendererItem>();
        LibVLC _libVLC;
        MediaPlayer _mediaPlayer;
        RendererDiscoverer _rendererDiscoverer;

        public Form_MAIN()
        {
            InitializeComponent();

        }

        private void AMC_OnError(object sender, AxAXISMEDIACONTROLLib._IAxisMediaControlEvents_OnErrorEvent e)
        {

        }

        private async void Button_DIFFUSER_ClickAsync(object sender, EventArgs e)
        {

            try
            {

                MessageBox.Show("1");
                DiscoverChromecasts();

                MessageBox.Show("2");
                await System.Threading.Tasks.Task.Delay(2000);

                MessageBox.Show("3");
                StartCasting();

                MessageBox.Show("4");
            }
            catch (Exception err)
            {
                MessageBox.Show("erreur : " + err);
            }
        }

        private void Form_MAIN_Load(object sender, EventArgs e)
        {

        }

        private void StartCasting()
        {
            // abort casting if no renderer items were found
            if (!_rendererItems.Any())
            {
                MessageBox.Show("No renderer items found. Abort casting...");
                return;
            }

            // create new media
            var media = new Media(_libVLC, "axrtsp://192.168.1.221/axis-media/media.amp", FromType.FromLocation);

            // create the mediaplayer
            _mediaPlayer = new MediaPlayer(_libVLC);

            // set the previously discovered renderer item (chromecast) on the mediaplayer
            // if you set it to null, it will start to render normally (i.e. locally) again
            _mediaPlayer.SetRenderer(_rendererItems.First());

            // start the playback
            _mediaPlayer.Play(media);
        }

        bool DiscoverChromecasts()
        {
            // load native libvlc libraries
            Core.Initialize();

            // create core libvlc object
            _libVLC = new LibVLC();

            // choose the correct service discovery protocol depending on the host platform
            // Apple platforms use the Bonjour protocol
            RendererDescription renderer;

            //if (Device.RuntimePlatform == Device.iOS)
            //    renderer = _libVLC.RendererList.FirstOrDefault(r => r.Name.Equals("Bonjour_renderer"));
            //else if (Device.RuntimePlatform == Device.Android)
            renderer = _libVLC.RendererList.FirstOrDefault(r => r.Name.Equals("microdns_renderer"));
            //else throw new PlatformNotSupportedException("Only Android and iOS are currently supported in this sample");

            // create a renderer discoverer
            _rendererDiscoverer = new RendererDiscoverer(_libVLC, renderer.Name);

            // register callback when a new renderer is found
            _rendererDiscoverer.ItemAdded += RendererDiscoverer_ItemAdded;

            // start discovery on the local network
            return _rendererDiscoverer.Start();
        }

        void RendererDiscoverer_ItemAdded(object sender, RendererDiscovererItemAddedEventArgs e)
        {
            MessageBox.Show($"New item discovered: {e.RendererItem.Name} of type {e.RendererItem.Type}");
            if (e.RendererItem.CanRenderVideo)
                MessageBox.Show("Can render video");
            if (e.RendererItem.CanRenderAudio)
                MessageBox.Show("Can render audio");

            // add newly found renderer item to local collection
            _rendererItems.Add(e.RendererItem);
        }


    }
}